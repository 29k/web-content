#!/bin/bash

set -euo pipefail

git submodule update --remote

echo "Installing firebase dependencies"
time yarn install --frozen-lockfile

echo "Deploy web"
yarn firebase -P production --token "${DEPLOY_FIREBASE_TOKEN}" \
  --non-interactive deploy --only hosting:content
