const COLORS = {
  BLACK: "#000000",
  BLACK_TRANSPARENT: "rgba(0, 0, 0, 0.5)",
  WHITE: "#FFFFFF",
  WHITE_TRANSPARENT: "rgba(255, 255, 255, 0.75)",
  //
  // Grey -- mostly typography
  //
  GREY100: "#FFFDF5",
  GREY200: "#E6E4DC",
  GREY300: "#CCCAC4",
  GREY400: "#B3B1AB",
  GREY500: "#999893",
  GREY600: "#807E7A",
  GREY700: "#666562",
  GREY800: "#595856",

  //
  // Peach -- mostly primary CTAs
  //
  PEACH100: "#FF7259",
  PEACH200: "#E65D45",
  PEACH300: "#CC533D",
  PEACH400: "#B34836",
  PEACH500: "#993E2E",
  PEACH600: "#803426",

  //
  // Light plum -- smaller text sections
  //
  LIGHT_PLUM10: "#AF9DB3",
  LIGHT_PLUM20: "#968799",
  LIGHT_PLUM30: "#7D7080",

  //
  // Plum -- Background shades
  //
  PLUM100: "#493C4D",
  PLUM200: "#3D3240",
  PLUM300: "#312833",
  PLUM400: "#251E26",
  PLUM500: "#18141A",
  PLUM600: "#0C0A0D",

  //
  // Secondary palette, eg illustrations
  //
  PETROL10: "#5A8199",
  PETROL20: "#3C5666",
  PETROL30: "#2B3541",

  RUST10: "#926F73",
  RUST20: "#7A5055",
  RUST30: "#6C4146",

  GREEN10: "#536D68",
  GREEN20: "#354D48",
  GREEN30: "#223D37",

  //
  // Exceptions -- Highlight alerts
  //
  ERROR_PINK: "#F85F7D",
  SUCCESS_GREEN: "#2CAC76",
  HANG_UP_RED: "#E65d45",

  USER_INPUT__INACTIVE: "#999893",
  USER_INPUT__ACTIVE: "#FFFDF5",
  USER_INPUT__DISABLED: "#807E7A",
};

export default COLORS;
