import {
  concat,
  find,
  map,
  propEq,
  prop,
  reduce,
  any,
  filter,
  length,
} from 'ramda';

export const getContentByLanguage = (content, language) =>
  map(prop(language))(content);

export const getCourseById = (courses, courseId) =>
  find(propEq('id', courseId), courses);

export const filterCoursesWithConversations = filter(({ lessons }) =>
  lessons.some((lesson) => 'conversations' in lesson),
);

export const getCourseForLessonId = (courses, lessonId) =>
  courses.find((course) =>
    course.lessons.find((lesson) => lesson.id == lessonId),
  );

export const getLessonById = (courses, lessonId) =>
  find(
    propEq('id', lessonId),
    reduce(concat, [], map(prop('lessons'), courses)),
  );

export const getLessonIndexById = (course, lessonId) => {
  if (course && 'lessons' in course) {
    return course.lessons.findIndex(({ id }) => id === lessonId);
  }
};

export const getLessonByIndex = (course, lessonIndex) => {
  if (course && 'lessons' in course) {
    return course.lessons[lessonIndex];
  }
};

export const getNextLessonId = (course, lessonId) => {
  if (course) {
    const index = getLessonIndexById(course, lessonId);
    if (index < 0) return;

    const lesson = getLessonByIndex(course, index + 1);

    if (!lesson) return;
    if (prop('wip', lesson)) return;

    return prop('id', lesson);
  }
};

export const getPrevLessonId = (course, lessonId) => {
  if (course) {
    const index = getLessonIndexById(course, lessonId);
    if (index > 0) {
      return prop('id', getLessonByIndex(course, index - 1));
    }
  }
};

export const getConversationById = (conversations, conversationId) =>
  find(propEq('id', conversationId), conversations);

export const getSectionById = (sections, sectionId) =>
  find(propEq('id', sectionId), sections);
