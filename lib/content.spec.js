/*
 * Copyright (c) 2018-2021 29k International AB
 */

const {
  getCourseById,
  getCourseForLessonId,
  getLessonById,
  getLessonIndexById,
  getLessonByIndex,
  getNextLessonId,
  getPrevLessonId,
} = require('./lesson');

const courses = [
  {
    id: 'first-course',
    name: 'First course',
    lessons: [
      {
        id: 'first-lesson',
        name: 'First lesson',
      },
      {
        id: 'second-lesson',
        name: 'Second lesson',
      },
      {
        id: 'third-lesson',
        name: 'Third lesson',
      },
      {
        id: 'fourth-lesson',
        name: 'Fourth lesson',
      },
    ],
  },
  {
    id: 'second-course',
    name: 'Second course',
    lessons: [
      {
        id: 'sixth-lesson',
        name: '2.1 lesson',
      },
      {
        id: 'seventh-lesson',
        name: '2.2 lesson',
      },
    ],
  },
  {
    id: 'third-course',
    name: 'Third course',
    lessons: [
      {
        id: 'first-lesson',
        name: '3.1 lesson',
      },
      {
        wip: true,
        id: 'second-lesson',
        name: '3.2 lesson',
      },
    ],
  },
];

describe('getCourseById', () => {
  it('gets first course by course id', () => {
    expect(getCourseById(courses, 'first-course')).toBe(courses[0]);
  });

  it('gets second course by course id', () => {
    expect(getCourseById(courses, 'second-course')).toBe(courses[1]);
  });

  it('returns undefined for a non-existing course id', () => {
    expect(getCourseById(courses, 'non-existing-id')).toBe(undefined);
  });
});

describe('getCourseForLessonId', () => {
  it('gets a course by lesson id', () => {
    expect(getCourseForLessonId(courses, 'third-lesson')).toBe(courses[0]);
  });
  it('returns undefined for a non-existing lesson id', () => {
    expect(getCourseForLessonId(courses, 'non-existing-id')).toBe(undefined);
  });
});

describe('getLessonById', () => {
  it('gets a lesson by lesson id', () => {
    expect(getLessonById(courses, 'third-lesson')).toBe(courses[0].lessons[2]);
  });

  it('returns undefined for a non-existing lesson id', () => {
    expect(getLessonById(courses, 'non-existing-id')).toBe(undefined);
  });
});

describe('getLessonIndexById', () => {
  it('gets a lesson index for course and lesson id', () => {
    expect(getLessonIndexById(courses[0], 'third-lesson')).toBe(2);
  });

  it('returns undefined for a non-existing course id', () => {
    expect(getLessonIndexById(undefined, 'third-lesson')).toBe(undefined);
  });

  it('returns -1 for a non-existing lesson id', () => {
    expect(getLessonIndexById(courses[0], 'non-existing-id')).toBe(-1);
  });
});

describe('getLessonByIndex', () => {
  it('gets a lesson by course and lesson index', () => {
    expect(getLessonByIndex(courses[0], 2)).toBe(courses[0].lessons[2]);
  });

  it('returns undefined for a non-existing course', () => {
    expect(getLessonByIndex(undefined, 2)).toBe(undefined);
  });

  it('returns undefined for a non-existing lesson index', () => {
    expect(getLessonByIndex(courses[0], 10)).toBe(undefined);
  });
});

describe('getNextLessonId', () => {
  it('returns next lesson for third lesson', () => {
    expect(getNextLessonId(courses[0], courses[0].lessons[2].id)).toBe(
      courses[0].lessons[3].id,
    );
  });
  it('returns undefined when course is done', () => {
    expect(getNextLessonId(courses[0], courses[0].lessons[3].id)).toBe(
      undefined,
    );
  });

  it('returns undefined when lesson is undefined', () => {
    expect(getNextLessonId(courses[0], undefined)).toBe(undefined);
  });

  it('returns undefined when course and lesson is undefined', () => {
    expect(getNextLessonId(undefined, undefined)).toBe(undefined);
  });

  it('returns undefined when next lesson is wip', () => {
    expect(getNextLessonId(courses[2], courses[2].lessons[0].id)).toBe(
      undefined,
    );
  });
});

describe('getPrevLessonId', () => {
  it('returns prev lesson for second lesson', () => {
    expect(getPrevLessonId(courses[0], courses[0].lessons[1].id)).toBe(
      courses[0].lessons[0].id,
    );
  });
  it('returns undefined for first lesson', () => {
    expect(getPrevLessonId(courses[0], courses[0].lessons[0].id)).toBe(
      undefined,
    );
  });
  it('returns undefined when lesson is undefined', () => {
    expect(getPrevLessonId(courses[0], undefined)).toBe(undefined);
  });
  it('returns undefined when course and lesson is undefined', () => {
    expect(getPrevLessonId(undefined, undefined)).toBe(undefined);
  });
});
