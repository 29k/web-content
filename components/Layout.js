import Head from "next/head";
import Link from "next/link";

import COLORS from "../constants/colors";

export default function Layout({ title = "29k Content", children }) {
  return (
    <main style={{ color: COLORS.WHITE, paddingBottom: "8rem" }}>
      <Head>
        <title>{title}</title>
      </Head>

      <nav
        style={{
          padding: "1rem",
          display: "flex",
          alignItems: "center",
        }}
      >
        <img
          src="/assets/icon.png"
          alt="29k Logo"
          style={{
            width: "48px",
            height: "48px",
            marginRight: "1rem",
            borderRadius: "50%",
          }}
        />

        <Link href="/">
          <a>Courses</a>
        </Link>
      </nav>

      <div style={{ width: "80vw", maxWidth: "1024px", margin: "0 auto" }}>
        {children}
      </div>
    </main>
  );
}
