import Link from "next/link";
import COLORS from "../constants/colors";

export default function Station({ href, title }) {
  return (
    <div
      style={{
        padding: ".5rem",
        background: COLORS.PLUM100,
        borderRadius: "16px",
        marginRight: "8px",
      }}
    >
      <Link href={href}>
        <a style={{ textDecoration: "none" }}>{title}</a>
      </Link>
    </div>
  );
}
