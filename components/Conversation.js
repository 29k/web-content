import {
  getContentByLanguage,
  getConversationById,
  getSectionById,
} from "../lib/content";
import ContentResolver from "./ContentResolver";

import { CONVERSATIONS } from "../content/content.json";

const conversations = getContentByLanguage(CONVERSATIONS, "en");

export default function Conversation({ conversationId, sectionId, path }) {
  const conversation = getConversationById(conversations, conversationId);
  const section = getSectionById(
    conversation.sections,
    sectionId ? sectionId : conversation.root
  );

  return <ContentResolver content={section.content} path={path} />;
}
