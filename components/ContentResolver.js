import Button from "./Content/Button";
import ButtonGroup from "./Content/ButtonGroup";
import HeaderImage from "./Content/HeaderImage";
import Image from "./Content/Image";
import Text from "./Content/Text";
import TextGroup from "./Content/TextGroup";

const Components = {
  Button,
  ButtonGroup,
  HeaderImage,
  Image,
  Text,
  TextGroup,
};

export default function ContentResolver({ content, path }) {
  return content.map(({ component, ...props }, index) => {
    if (component in Components) {
      const Component = Components[component];
      return <Component {...props} path={path} key={index} />;
    }

    return <p>Component not found {component}</p>;
  });
}
