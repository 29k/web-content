import ContentResolver from '../ContentResolver';

export default function ButtonGroup({ content, path }) {
  return <ContentResolver content={content} path={path} />;
}
