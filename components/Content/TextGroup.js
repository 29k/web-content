import ContentResolver from '../ContentResolver';

export default function TextGroup({ content, path }) {
  return <ContentResolver content={content} path={path} />;
}
