import Link from "next/link";
import COLORS from "../../constants/colors";

export default function Button({ content, sectionId, path }) {
  return (
    <Link href={`${path}/section/${sectionId}`}>
      <a
        style={{
          padding: ".5rem",
          background: COLORS.PEACH400,
          borderRadius: "16px",
          marginRight: "8px",
          textDecoration: "none",
        }}
      >
        {content}
      </a>
    </Link>
  );
}
