export default function Image({ content }) {
  return <img src={content.url} style={{ maxWidth: '100%' }} />;
}
