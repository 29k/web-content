export default function HeaderImage({ content }) {
  return <img src={content.uri} style={{ maxWidth: '100%' }} />;
}
