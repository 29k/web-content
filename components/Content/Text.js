import ReactMarkdown from 'react-markdown';

export default function Text({ content }) {
  return <ReactMarkdown>{content}</ReactMarkdown>;
}
