import Link from "next/link";

import { COURSES } from "../content/content.json";
import {
  getContentByLanguage,
  filterCoursesWithConversations,
} from "../lib/content";
import Layout from "../components/Layout";

const filteredCourses = filterCoursesWithConversations(
  getContentByLanguage(COURSES, "en")
);

export default function Home() {
  return (
    <Layout>
      <h1>Courses</h1>
      <ul style={{ display: "grid", gridTemplateColumns: "1fr", gap: "1rem" }}>
        {filteredCourses.map((c) => (
          <li key={c.id}>
            <Link href={`/course/${c.id}`}>
              <a>{c.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
}
