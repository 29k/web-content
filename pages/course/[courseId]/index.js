import Link from "next/link";

import { COURSES } from "../../../content/content.json";
import {
  getContentByLanguage,
  getCourseById,
  filterCoursesWithConversations,
} from "../../../lib/content";
import Layout from "../../../components/Layout";

const courses = getContentByLanguage(COURSES, "en");

export default function Course({ course }) {
  return (
    <Layout title={course.name}>
      <h1>{course.name}</h1>

      <ul style={{ display: "grid", gridTemplateColumns: "1fr", gap: "1rem" }}>
        {course.lessons.map((lesson) => (
          <li key={lesson.id}>
            <Link href={`/course/${course.id}/lesson/${lesson.id}`}>
              <a>{lesson.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </Layout>
  );
}

export function getStaticPaths() {
  const paths = filterCoursesWithConversations(courses).map((course) => ({
    params: { courseId: course.id },
  }));

  return {
    paths,
    fallback: false,
  };
}

export function getStaticProps({ params }) {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  const course = getCourseById(courses, params.courseId);

  // Pass post data to the page via props
  return {
    props: { course },
    // Re-generate the post at most once per second
    // if a request comes in
    revalidate: 1,
  };
}
