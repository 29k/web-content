import Link from "next/link";
import { head, prop } from "ramda";

import Layout from "../../../../../../../../../components/Layout";
import Conversation from "../../../../../../../../../components/Conversation";
import Station from "../../../../../../../../../components/Station";

import {
  COURSES,
  CONVERSATIONS,
} from "../../../../../../../../../content/content.json";
import {
  getContentByLanguage,
  getCourseById,
  getLessonById,
  getConversationById,
  filterCoursesWithConversations,
} from "../../../../../../../../../lib/content";

const courses = getContentByLanguage(COURSES, "en");
const conversations = getContentByLanguage(CONVERSATIONS, "en");

export default function Lesson({ course, lesson, conversationId, sectionId }) {
  const conversationName = lesson.conversations.find(
    (conversation) => conversation.conversationId === conversationId
  ).name;
  return (
    <Layout title={`${conversationName} | ${lesson.name} | ${course.name}`}>
      <h1>{course.name}</h1>
      <h2>{lesson.name}</h2>
      <h3>{conversationName}</h3>

      <div style={{ display: "flex" }}>
        {lesson.conversations.map((conversation) => (
          <Station
            href={`/course/${course.id}/lesson/${lesson.id}/conversation/${conversation.conversationId}`}
            key={conversation.conversationId}
            title={conversation.title}
          />
        ))}
      </div>

      <Conversation
        conversationId={conversationId}
        sectionId={sectionId}
        path={`/course/${course.id}/lesson/${lesson.id}/conversation/${conversationId}`}
      />
    </Layout>
  );
}

export function getStaticPaths() {
  const paths = filterCoursesWithConversations(courses).reduce(
    (paths, { id: courseId, lessons = [] }) => [
      ...paths,
      ...lessons.reduce(
        (paths, { id: lessonId, conversations: lessonConversations = [] }) => [
          ...paths,
          ...lessonConversations.reduce((paths, { conversationId }) => {
            const conversation = getConversationById(
              conversations,
              conversationId
            );
            const { sections } = conversation;

            return [
              ...paths,
              ...sections.map(({ id: sectionId }) => ({
                params: {
                  courseId,
                  lessonId,
                  conversationId,
                  sectionId,
                },
              })),
            ];
          }, []),
        ],
        []
      ),
    ],
    []
  );

  return {
    paths,
    fallback: false,
  };
}

export function getStaticProps({ params }) {
  const { courseId, lessonId, conversationId, sectionId } = params;

  const course = getCourseById(courses, courseId);
  const lesson = getLessonById(courses, lessonId);

  // Pass post data to the page via props
  return {
    props: { course, lesson, conversationId, sectionId },
    // Re-generate the post at most once per second
    // if a request comes in
    revalidate: 1,
  };
}
