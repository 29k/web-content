import { head, prop } from "ramda";

import Conversation from "../../../../../components/Conversation";
import Layout from "../../../../../components/Layout";
import Station from "../../../../../components/Station";

import { COURSES } from "../../../../../content/content.json";
import {
  getContentByLanguage,
  getCourseById,
  getLessonById,
  filterCoursesWithConversations,
} from "../../../../../lib/content";

const courses = getContentByLanguage(COURSES, "en");

export default function Lesson({ course, lesson }) {
  const conversationId = prop(
    "conversationId",
    head(lesson.conversations || [])
  );
  return (
    <Layout title={`${lesson.name} | ${course.name}`}>
      <h1>{course.name}</h1>
      <h2>{lesson.name}</h2>
      <div style={{ display: "flex" }}>
        {lesson.conversations.map((conversation) => (
          <Station
            href={`/course/${course.id}/lesson/${lesson.id}/conversation/${conversation.conversationId}`}
            key={conversation.conversationId}
            title={conversation.title}
          />
        ))}
      </div>

      <Conversation
        conversationId={conversationId}
        path={`/course/${course.id}/lesson/${lesson.id}/conversation/${conversationId}`}
      />
    </Layout>
  );
}

export function getStaticPaths() {
  const paths = filterCoursesWithConversations(courses).reduce(
    (paths, { id: courseId, lessons = [] }) => [
      ...paths,
      ...lessons.map(({ id: lessonId }) => ({
        params: {
          courseId,
          lessonId,
        },
      })),
    ],
    []
  );

  return {
    paths,
    fallback: false,
  };
}

export function getStaticProps({ params }) {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  const course = getCourseById(courses, params.courseId);
  const lesson = getLessonById(courses, params.lessonId);

  // Pass post data to the page via props
  return {
    props: { course, lesson },
    // Re-generate the post at most once per second
    // if a request comes in
    revalidate: 1,
  };
}
